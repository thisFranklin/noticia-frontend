import React, { useState, useEffect } from "react";
import { Input } from "../../components/forms/Input";
import axios from "axios"


export function LoginPage() {

  const [username, setUsername] = useState("")
  const [password, setPassword] = useState("")

  const handleSubmit = (e)=>{
    e.preventDefault();
    console.log(username, password)
  }

  useEffect(()=>{
    async function fetchNotes(){
      const {data} = await axios.get("/notes/")
      console.log(data)
    }
  }, [])



  return (
    <div className="h-screen m-0 flex flex-col justify-center place-items-center bg-gradient-to-br from-slate-200 via-slate-100 to-slate-50">
      <div className="text-3xl text-blue-700 tracking-widest pb-12 font-bold">
        Noticia
      </div>
      <form onSubmit={handleSubmit} className="flex flex-col lg:w-96 md:w-80 mb-48">
        <Input htmlFor="username" type="text" onChange={(e)=>setUsername(e.target.value)} />

        <Input htmlFor="password" type="password" onChange={(e)=>setPassword(e.target.value)} />
        
        <button type="submit" className="w-full h-[42px] mt-4 font-semibold tracking-wider text-xl text-white rounded-md bg-gradient-to-br from-blue-700 to-blue-400 hover:to-blue-500 hover:shadow-none shadow-2xl">Connexion</button>
      </form>
    </div>
  );
}
