import React from "react";

function fLUpperCase(str){
    return str.charAt(0).toUpperCase() + str.slice(1)
}

export function Input({ htmlFor, type, value="", onChange }) {
  return (
    <>
      <label htmlFor={htmlFor} className="mb-2 font-medium">{fLUpperCase(htmlFor)}</label>

      <input type={type} value={value} id={htmlFor} name={htmlFor} onChange={onChange} className="mb-4 px-2 h-[40px] ring-2 ring-slate-600 focus:outline-blue-700 focus:ring-0 rounded-md appearance-none" />
    </>
  );
}