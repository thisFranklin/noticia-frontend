import { createBrowserRouter, createRoutesFromElements, Route } from "react-router-dom"
import {LoginPage} from "../pages/LoginPage"


export const router = createBrowserRouter(
    createRoutesFromElements(
        <Route>
            <Route path="/" element= {<LoginPage/>} />
        </Route>
    )
  )

